#! python3

import discord
import os
from discord.ext import commands, tasks
import logging
from itertools import cycle

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s -  %(levelname)s -  %(message)s')

logging.disable(logging.CRITICAL)

client = commands.Bot(command_prefix = '.')

@client.event
async def on_ready():
    print('Bot is online')


@client.command()
@commands.has_permissions(administrator=True)
async def load(ctx, extension):
    client.load_extension(f'cogs.{extension}')

@client.command()
@commands.has_permissions(administrator=True)
async def unload(ctx, extension):
    client.unload_extension(f'cogs.{extension}')

@client.command()
async def reload(ctx, extension):
    client.unload_extension(f'cogs.{extension}')
    client.load_extension(f'cogs.{extension}')
    print(f'Reloaded {extension}')
    await ctx.send(f'Reloaded {extension}')

for filename in os.listdir('./cogs'):
    if filename.endswith('.py'):
        client.load_extension(f'cogs.{filename[:-3]}')
        logging.debug(filename)

client.run('NzIxNzc1NjU0MzQ0OTgyNTg5.XuZfBQ.4Hv2XqBambp8_WemSi9coUphOEA')

import discord
from discord.ext import commands, tasks

class BBotCommands(commands.Cog):
    def __init__(self, client):
        self.client = client


    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send('Please pass in all required arguments.')
        elif isinstance(error, commands.CommandNotFound):
            await ctx.send('Command not found. Type \'.help\'')


    #Commands
    ## Get server ping command
    @commands.command()
    @commands.has_permissions(administrator=True)
    async def ping(self, ctx):
        await ctx.send(f'Pong! {round(self.client.latency * 1000)}ms')

    ## Kick command
    @commands.command()
    @commands.has_permissions(administrator=True)
    async def kick(self, ctx, memeber : discord.Member, *, reason=None):
        await member.kick(reason=reason)
        await ctx.send(f'Kicked {member.mention}')

    ## Ban Command
    @commands.command()
    @commands.has_permissions(administrator=True)
    async def ban(self, ctx, memeber : discord.Member, *, reason=None):
        await member.ban(reason=reason)
        await ctx.send(f'Banned {member.mention}')

    ## Unban command
    @commands.command()
    @commands.has_permissions(administrator=True)
    async def unban(self, ctx, *, member):
        banned_users = await ctx.guild.bans()
        member_name, member_discriminator = member.split('#')
        for ban_entry in banned_users:
            user = ban_entry.user
            if (user.name, user.discriminator) == (member_name, member_discriminator):
                await ctx.guild.unban(user)
                await ctx.send(f'Unbanned {user.name}#{user.discriminator}')
                return

    ## Hot mic commands to remove user from voice channel and message them
    @commands.command(aliases=['hm'])
    @commands.has_permissions(administrator=True)
    async def hotmic(self, ctx, member : discord.Member): #hot mic command to mute and message whoever hot mics
        await member.move_to(channel=None, reason=None)
        await member.send(f'{member}, your mic is hot right now and you were muted to cool off (wear some headphones please). ')

    ## Clear messages from discord channel
    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def clear(self, ctx, amount=10):
        await ctx.channel.purge(limit=amount)

def setup(client):
    client.add_cog(BBotCommands(client))
